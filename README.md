Native Android SDK for conversion tracking

## Getting Started

### Add SDK as dependency


```groovy
allprojects {
		repositories {
			
			maven { url 'https://jitpack.io' }
		}
	}

dependencies {
 	implementation 'com.gitlab.terragonengineering:android-conversion-tracking:1.0'
 }
```
 
### Add Api Key
Add the api key provided to you to your `AndroidManifiest.xml` file under the `application` tag as shown below assuming your api key is `appKey`
```xml
 <application
        android:allowBackup="true"
        android:name=".YourAweSomeApplication"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/AppTheme">

        <meta-data android:name="com.terragonltd.android.API_KEY"
            android:value="your_api_key"/>

        <activity android:name=".MainActivity">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
    </application>
```

### Usage
#### Initialising SDK
Initialize the sdk in your main application as show below
 ```java
import com.terragonltd.tracking.android.TerragonTracking;

public class YourAweSomeApplication extends Application {
        @Override
        public void onCreate() {
            super.onCreate();
            TerragonTracking.bootstrap(this);
            // your other codes
            
        }
    //...
    
}
```
#### Tracking events
```java

import com.terragonltd.tracking.android.TerragonTracking;
import com.terragonltd.tracking.android.TrackingEvents;
import com.terragonltd.tracking.android.TrackingParameters;

public class YourActivity{
    public void yourClickFunction(){
        //track purchase
        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(TrackingParameters.REVENUE,1234.56);
        eventValue.put(TrackingParameters.CONTENT_TYPE,"Airtime");
        eventValue.put(TrackingParameters.CONTENT_ID,"1234567");
        eventValue.put(TrackingParameters.CURRENCY,"NGN");
        
        TerragonTracking.getInstance(getApplicationContext()).trackEvent(TrackingEvents.PURCHASE, eventValue);
    
    }
    
    public void trackRegistration(){
            //track registration
            Map<String, Object> eventValue = new HashMap<String, Object>();
            eventValue.put(TwinpineParameters.CONTENT_ID,"your_user_id");
            //fill free to add custom fields
            eventValue.put("user_email","your_user_email_address");
            Twinpine.getInstance(this).trackEvent(getApplicationContext(),
            TwinpineEvents.REGISTRATION , eventValue);    
        }
}
