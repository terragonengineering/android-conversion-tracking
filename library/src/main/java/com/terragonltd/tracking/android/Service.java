package com.terragonltd.tracking.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


class Service {

    private static final String SHARED_PREFERENCES_NAME="terragon_tracking";

    static JSONObject getMsisdn(){

        /*TelephonyManager tMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();*/


        Log.d("TerragonTracking", "get msisdn");

        OkHttpClient client = new OkHttpClient();

        String url ="http://msisdn.terragonbase.com/tmoniF/get-msisdn";

        Request request = new Request.Builder()
                .url(url)
                .build();
        try{
            Log.d("TerragonTracking", "try");
            Response response = client.newCall(request).execute();

            Log.d("TerragonTracking", "try done");
            JSONObject r = new JSONObject(response.body().string());

            return r.getJSONObject("response");
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    static boolean isWifiConnected(Context context) {
        WifiManager wifiMgr = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (wifiMgr!= null && wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
            return  wifiInfo.getNetworkId() != -1;
        }
        else {
            return false; // Wi-Fi adapter is OFF
        }
    }

    public static void storeString(Context context, String key, String value){
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key,value);
        editor.apply();
    }

    public static String getStoredString(Context context, String key){
        SharedPreferences pref = context.getSharedPreferences(SHARED_PREFERENCES_NAME, 0);
        return pref.getString(key, null);
    }




}
