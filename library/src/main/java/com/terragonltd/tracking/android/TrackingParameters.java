package com.terragonltd.tracking.android;

public class TrackingParameters {
    public static String REVENUE = "revenue";
    public static String CONTENT_TYPE = "content_type";
    public static String CONTENT_ID = "content_id";
    public static String CURRENCY = "currency";
}
