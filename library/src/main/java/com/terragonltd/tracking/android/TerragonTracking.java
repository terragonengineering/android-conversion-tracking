package com.terragonltd.tracking.android;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TerragonTracking {

    private Context context;

    private static String apiKey;
    private static String googleAdid;


    public TerragonTracking(Context context){
        this.context = context;
    }

    public void trackEvent(final String eventName, final Map<String,Object> params){
        Log.d("TerragonTracking", "send event "+eventName);
        Log.d("TerragonTracking", googleAdid);
        new PostbackTask(context, eventName, params).execute();

    }

    public static void bootstrap(Context context){
        try{
            Bundle appBundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData;
            apiKey = appBundle.getString("com.terragonltd.android.API_KEY");
        } catch (Exception e){
            e.printStackTrace();
        }

        String referralString = Service.getStoredString(context, "referrer_data");
        if (referralString == null){
            getInstallReferral(context);
        } else {
            getGoogleAdvertisingId(context);
        }
    }

    private static void getInstallReferral(final Context context){

        final InstallReferrerClient referrerClient = InstallReferrerClient.newBuilder(context).build();
        referrerClient.startConnection(new InstallReferrerStateListener() {
            @Override
            public void onInstallReferrerSetupFinished(int responseCode) {
                switch (responseCode) {
                    case InstallReferrerClient.InstallReferrerResponse.OK:
                        // Connection established.
                        try{
                            ReferrerDetails response = referrerClient.getInstallReferrer();
                            String referrerUrl = response.getInstallReferrer();
                            long referrerClickTime = response.getReferrerClickTimestampSeconds();
                            long appInstallTime = response.getInstallBeginTimestampSeconds();

                            JSONObject referrerData = new JSONObject();
                            referrerData.put("url", referrerUrl);
                            referrerData.put("click_time", referrerClickTime);
                            referrerData.put("install_time", appInstallTime);

                            Service.storeString(context, "referrer_data", referrerData.toString());
                            getGoogleAdvertisingId(context);

                        } catch(Exception e){
                            e.printStackTrace();
                        }

                        break;
                    default:

                }
            }

            @Override
            public void onInstallReferrerServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });
    }

    private static void getGoogleAdvertisingId(final Context context) {
        Log.d("TerragonTracking", "google ad");

        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                AdvertisingIdClient.Info idInfo = null;
                try {
                    idInfo = AdvertisingIdClient.getAdvertisingIdInfo(context);
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String advertId = null;
                try{
                    advertId = idInfo.getId();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }

                return advertId;
            }

            @Override
            protected void onPostExecute(String advertId) {
                googleAdid = advertId;
                sendAppOpenedEvent(context);
                sendAppInstallEvent(context);

                Log.d("TerragonTracking", "google ad id");
                Log.d("TerragonTracking", googleAdid);

            }

        };
        task.execute();
    }


    private static void sendAppOpenedEvent(Context context){
        getInstance(context).trackEvent("app_opened", null);
    }

    private static void sendAppInstallEvent(Context context){
        try {
            if (Service.getStoredString(context, "install_event_sent") != null){
                final String referrerData = Service.getStoredString(context, "referrer_data");

                if (referrerData != null){
                    String referrerUrl = new JSONObject(referrerData).getString("url");
                    if (referrerUrl.contains("terragon")){
                        getInstance(context).trackEvent("install", null);
                    }
                }
            }


        } catch (Exception e){

        }


    }

    public static TerragonTracking getInstance(Context context){
        return new TerragonTracking(context);
    }

    private static class PostbackTask extends AsyncTask<Void, Void, String> {

        private WeakReference<Context> contextReference;

        String eventName;
        Map params;
        PostbackTask(Context context, String eventName, Map params) {
            contextReference = new WeakReference<>(context);
            this.eventName = eventName;
            this.params = params;
        }

        @Override
        protected String doInBackground(Void... _params) {
            OkHttpClient client = new OkHttpClient();
            try{
                final MediaType JSON
                        = MediaType.get("application/json; charset=utf-8");

                final String referrerData = Service.getStoredString(contextReference.get(), "referrer_data");


                JSONObject requestData = new JSONObject();
                requestData.put("event", eventName);
                if (params != null)
                    requestData.put("eventParams", new JSONObject(params));
                requestData.put("app_referrer", new JSONObject(referrerData));
                requestData.put("app_os", "android");
                requestData.put("advertising_id", googleAdid);

                JSONObject msisdnInfo = Service.getMsisdn();
                if (msisdnInfo != null){
                    requestData.put("msisdn", msisdnInfo.get("msisdn"));
                    requestData.put("operator", msisdnInfo.get("operator"));
                    requestData.put("ip", msisdnInfo.get("ip"));

                }
                requestData.put("wifi_on", Service.isWifiConnected(contextReference.get()));

                Log.d("TerragonTracking", "do request");

                RequestBody body = RequestBody.create(requestData.toString(), JSON);
                Request request = new Request.Builder()
                        .url("https://staging-postback.twinpinenetwork.com/app")
                        .post(body)
                        .addHeader("x-api-key", apiKey)
                        .build();

                Response response = client.newCall(request).execute();

                return response.body().string();

            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String response) {

            Log.d("TerragonTracking", "request done");
            if (response != null)
                Log.d("TerragonTracking", response);

            if (response != null && eventName.equals("install")){
                Service.storeString(contextReference.get(), "install_event_sent", "1");
            }
        }
    }

}
