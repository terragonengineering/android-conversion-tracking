package com.terragonltd.tracking.android;

public class TrackingEvents {
    public static String PURCHASE = "purchase";
    public static String REGISTRATION = "registration";
    public static String AIRTIME = "airtime";
    public static String BILL = "bill";
    public static String DEPOSIT = "deposit";
    public static String TRANSFER = "transfer";
}
